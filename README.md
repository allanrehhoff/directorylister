# Project DirectoryLister #

New an intuitive way of displaying all your files on your webserver.

## Features ##
* Responsive
* Upload foiles (Multiupload on todo)
* Password protect uploads
* Prevents directory traversal when downloading files from link
* Database free.
* Easy to set up.
* Sort files uploaded by data
* Sort viewing of files
* Object Oriented
* Exclude files and folders from bieng viewed.

## TODO ##
* Add ability to upload to current viewed directory
* Finish upload of multiple files
