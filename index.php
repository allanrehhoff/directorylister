<?php
    require('resources/DirectoryLister.php');
	
	$requestDir = isset($_GET['dir']) ? $_GET['dir'] : '.';
    $lister = new DirectoryLister($requestDir);

    if (isset($_GET['hash'])) {
        $hashes = $lister->getFileHash($_GET['hash']);
        $data   = json_encode($hashes);

        die($data);
    }

	if(isset($_POST['upload'])) {
		if($lister->authenticate($_POST['passcode'])) {
			$files = $lister->reArrangeFiles($_FILES['files']);
			foreach($files as $file) {
				if($lister->validateFile($file)) {
					if($lister->upload($file['tmp_name'], $file['name'])) {
						$lister->setSystemMessage('success', 'Thank you! Your file '.$file['name']. ' was uploaded without trouble. <a href="'.$lister->httpUrl.'" onClick="openLinkPathDialog(jQuery(this)); return false;">Get the link to your file.</a>');
					} else {
						$lister->setSystemMessage('danger', 'The file could not be uploaded, server side error.');
					}
				} else {
					$lister->eventLog('Unvalidated file: '.$file['name']);
					$lister->setSystemMessage('danger', 'Unable to upload file could not be validated as safe.');
				}
			}
		} else {
			$lister->eventLog('Incorrect passsword provided.');
			$lister->setSystemMessage('danger', 'Darn it! file could not be upload because password was incorrect.');
		}
	}
	
	if(isset($_GET['download'])) {
		try {
			$lister->serve($_GET['download']);
		} catch(Exception $e) {
			$lister->setSystemMessage('danger', $e->getMessage());
		}
	} //else {
		//$lister->listDirectory($requestDir);
		
		if (!defined('THEMEPATH')) {
			define('THEMEPATH', $lister->getThemePath());
		}

		$themeIndex = $lister->getThemePath(true) . '/index.php';

		if (file_exists($themeIndex)) {
			require($themeIndex);
		} else {
			die('ERROR: Failed to initialize theme');
		}
<<<<<<< HEAD
    //}
=======
    //}
>>>>>>> origin/master
