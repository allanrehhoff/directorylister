jQuery(document).ready(function($) {

    // Get page-content original position
    var contentTop = $('#page-content').offset().top;

    // Show/hide top link on page load
    showHideTopLink(contentTop);

    // Show/hide top link on scroll
    $(window).scroll(function() {
        showHideTopLink(contentTop);
    });

    // Scroll page on click action
    $('#page-top-link').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    });

	$("[rel='tooltip']").tooltip();
   
    // Hash button on click action
    $('.file-info-button').click(function(event) {

        // Get the file name and path
        var name = $(this).closest('li').attr('data-name');
        var path = $(this).closest('li').attr('data-href');

        // Set modal title value
        $('#file-info-modal .modal-title').text(name);

        $('#file-info .md5-hash').text('Loading...');
        $('#file-info .sha1-hash').text('Loading...');
        $('#file-info .filesize').text('Loading...');

        $.ajax({
            url:     '?hash=' + path,
            type:    'get',
            success: function(data) {

                // Parse the JSON data
                var obj = jQuery.parseJSON(data);


                // Set modal pop-up hash values
                $('#file-info .md5-hash').text(obj.md5);
                $('#file-info .sha1-hash').text(obj.sha1);
                $('#file-info .filesize').text(obj.size);

            }
        });

        // Show the modal
        $('#file-info-modal').modal('show');

        // Prevent default link action
        event.preventDefault();

    });
    $('input[type="file"]').change(function() {
		if($.trim($(this).val()) === '') {
			$('#filePath').val('No file selected');
			return;
		} 
		
		var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
		$('#filePath').val(filename);
		$('#form-upload-modal').modal('show');
		
		setTimeout(function() { $('#passcode').focus() }, 400);
		
		$('#form-upload').on('submit', function() {
			$(this).find('#password-modal-btn, #passcode').fadeOut('fast', function() {
				$('#loader').fadeIn();
				setTimeout(function() {
					$('#patience').fadeIn();
				}, 5000);
			});
		});
	});
});

function openLinkPathDialog(element) {
	$('#file-path-modal').find('#pathway').val(element.attr('href'));
	$('#file-path-modal').modal('show');
}

function showHideTopLink(elTop) {
    if($('#page-navbar').offset().top + $('#page-navbar').height() >= elTop) {
        $('#page-top-nav').show();
    } else {
        $('#page-top-nav').hide();
    }
}
