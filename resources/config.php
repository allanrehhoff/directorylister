<?php

return array(

    // Basic settings
    'hide_dot_files'     => true,
    'list_folders_first' => true,
    'list_sort_order'    => 'natcasesort',
    'theme_name'         => 'bootstrap',
	'allow_overwrite'     => false,
	
	//Prevent the following file types from being uploaded.
	'blacklist' => array ( //either 'blacklist' or 'whitelist' Change to whitelist if you don't want everything.
		'php',
		'php4',
		'php5',
		'htaccess',
		'exe'
	),
	
	//upload settings
	'upload_dir' 			=> '/Uploads',
	'max_upload_size' 		=> 104857600, //100 MB - You should propably change this to a lower value.
	'upload_password' 		=> '0401',
	'group_uploads_by_date' => true,
	'move_torrent_files'    => '/home/rehhoff/torrents/watch/', //enter full system path to where torrent files should be moved 
																//eg. /torrents/watch, false to disable
    // Hidden files
    'hidden_files' => array(
        '*/.ht*',
        'resources',
        'resources/*',
        'analytics.inc',
		'cron',
		'sandbox',
		'.',
		'index.php',
		'transfer',
		'transfer/*'
    ),

    // File hashing threshold
    'hash_size_limit' => 268435456, // 256 MB

    // Custom sort order
    'reverse_sort' => array(
        // 'path/to/folder'
    )

);
