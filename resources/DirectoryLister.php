<?php

/**
 * A simple PHP based directory lister that lists the contents
 * of a directory and all it's sub-directories and allows easy
 * navigation of the files within.
 */
class DirectoryLister {

    // Define application version
    const VERSION = '3.0';
    const CRLF = "\r\n";

    // Reserve some variables
    protected $themeName     = null;
    protected $directory     = null;
    protected $appDir        = null;
    protected $appUrl        = null;
    protected $config        = null;
    protected $fileTypes     = null;
    protected $systemMessage = null;

	public $httpUrl;
	public $directory_list 	= null;
	
	/**
     * DirectoryLister construct function. Runs on object creation.
     */
    public function __construct($initializedirectory) {

        // Set class directory constant
        if(!defined('__DIR__')) {
            define('__DIR__', dirname(__FILE__));
        }

        // Set application directory
        $this->appDir = __DIR__;

        $this->appUrl = $this->getAppUrl();

        $configFile = $this->appDir . '/config.php';

        // Set the config array to a global variable
        if (file_exists($configFile)) {
            $this->config = require($configFile);
        } else {
            $this->setSystemMessage('danger', '<b>ERROR:</b> Unable to locate application config file');
        }

        // Set the file types array to a global variable
        $this->fileTypes = require($this->appDir . '/fileTypes.php');

        // Set the theme name
        $this->themeName = $this->config['theme_name'];
		
		if($initializedirectory) {
			$this->listDirectory($initializedirectory);
		}
    }

    public function eventLog($msg) {
        $datetime = new DateTime();
        $eventString = "[".$datetime->format('Y-m-d h:i:s').'] ['.$_SERVER['REMOTE_ADDR'].'] '.trim($msg).self::CRLF;
        $logfile = $this->appDir.'/upload.log';

        return file_put_contents($logfile, $eventString, FILE_APPEND);
    }

    /**
     * Creates the directory listing and returns the formatted XHTML
     *
     * @param string $path Relative path of directory to list
     * @return array Array of directory being listed
     * @access public
     */
    public function listDirectory($directory) {
        $directory = $this->setDirectoryPath($directory);

        if ($directory === null) {
            $directory = $this->directory;
        }

        $directoryArray = $this->readDirectory($directory);

		$this->directory_list = $directoryArray;
        return $directoryArray;
    }


    /**
     * Parses and returns an array of breadcrumbs
     *
     * @param string $directory Path to be breadcrumbified
     * @return array Array of breadcrumbs
     * @access public
     */
    public function listBreadcrumbs($directory = null) {
        if ($directory === null) {
            $directory = $this->directory;
        }

        $dirArray = explode('/', $directory);

        $breadcrumbsArray[] = array(
            'link' => $this->appUrl,
            'text' => 'Home'
        );

        foreach ($dirArray as $key => $dir) {
            if ($dir != '.') {
                $dirPath  = null;

                // Build the directory path
                for ($i = 0; $i <= $key; $i++) {
                    $dirPath = $dirPath . $dirArray[$i] . '/';
                }

                // Remove trailing slash
                if(substr($dirPath, -1) == '/') {
                    $dirPath = substr($dirPath, 0, -1);
                }

                // Combine the base path and dir path
                $link = $this->appUrl . '?dir=' . urlencode($dirPath);

                $breadcrumbsArray[] = array(
                    'link' => $link,
                    'text' => $dir
                );
            }
        }
        return $breadcrumbsArray;
    }


    /**
     * Get path of the listed directory
     *
     * @return string Path of the listed directory
     * @access public
     */
    public function getListedPath() {
        if ($this->directory == '.') {
            $path = $this->appUrl;
        } else {
            $path = $this->appUrl . $this->directory;
        }

        return $path;
    }
	
	public function convertBytes($bytes, $round = 2, $power = 1024) {
		 // human readable format -- powers of 1000
		$unit = array("B", "KB", "MB", "GB", "TB", "PB", "EB", "YB");
		return round($bytes / pow($power, ($i = floor(log($bytes, $power)))), $round).' '.$unit[$i];
	}

    /**
     * Returns the theme name.
     *
     * @return string Theme name
     * @access public
     */
    public function getThemeName() {
        return $this->config['theme_name'];
    }
	/*
	* Performs a quick check on the password user submitted upon upload.
	*
	* @param The user submitted password
	* @return bool
	* @acess public
	*/
	public function authenticate($code) {
		return ($this->config['upload_password'] == $code);
	}
	
    /*
    * Get the asumed file extension depending on filename
    * @param A file name
    * @return string
    */
	public function getExt($filename) {
		$fileparts = explode('.', $filename);
		$extension = end($fileparts);
		return strtolower($extension);
	}
	
	/*
	* Checks if a file is a valid file according to configs
	*
	* @param file
	* @return bool
	* @access public
	*/

	public function validateFile($file) {
		$ext = $this->getExt($file['name']);
		
		if($file['size'] > $this->config['max_upload_size']) {
			$limit =  $this->convertBytes($this->config['max_upload_size']);
			$filesize = $this->convertBytes($file['size']);
			$this->setSystemMessage('danger', 'Attachment size ('.$filesize.') exceeded the allowed limit of '.$limit);
			return false;
		}
		
		if(isset($this->config['whitelist'])) {
			return in_array($ext, $this->config['whitelist']);
		} elseif(isset($this->config['blacklist'])) {
			return !in_array($ext, $this->config['blacklist']);
		} else {
			return false;
			$this->setSystemMessage('warning', 'No upload filter is defined in config.php');
		}
	}
	/*
	* Ensures a filename is unique
	*/
	protected function incrementFilename($path, $filename){
		if ($pos = strrpos($filename, '.')) {
			   $name = substr($filename, 0, $pos);
			   $ext = substr($filename, $pos);
		} else {
			   $name = $filename;
		}

		$newpath = $path.'/'.$filename;
		$newname = $filename;
		$counter = 1;
		while (file_exists($newpath)) {
			   $newname = $name .'('.$counter.')'.$ext;
			   $newpath = $path.'/'.$newname;
			   $counter++;
		 }

		return $newname;
	}
	/*
	* Re arrange a $_FILES array to a more logical structure..
	*/
	public function reArrangeFiles($filePost) {
		$fileAry = array();
		$fileCount = count($filePost['name']);
		$fileKeys = array_keys($filePost);

		for ($i=0; $i < $fileCount; $i++) {
			foreach ($fileKeys as $key) {
				$fileAry[$i][$key] = $filePost[$key][$i];
			}
		}

		return $fileAry;
	}

	/*
	* Serves a given file path to the client
	*/
	public function serve($filePath) {
		$basepath = getcwd();
		$realBase = realpath($basepath);
		
		$filePath = $basepath.'/'.$filePath;
		$realUserPath = realpath($filePath);

		if($realUserPath === false || strpos($realUserPath, $realBase) !== 0) {
			$this->eventLog('failed to download '.$filePath.' possible directory traversal detected.');
            throw new Exception('Unable to serve the requested file, maybe it does not exist..');
		}
	
		//$filePath = $basepath.'/'.$filePath;
 
		if (file_exists($filePath) && is_file($filePath)) {
			if($this->_isHidden($filePath)) {
                $this->eventLog('Tried to download hidden file '.$filePath);
                throw new Exception('Unable to serve the requested file, maybe it does not exist.');
			}
			
			//header('Content-Description: File Transfer');
		    header('Content-Type: '.mime_content_type($filePath));
		
			$fileParts = explode('/', $filePath);
		    $filename = end($fileParts);
		    
		    if (strstr($_SERVER['HTTP_USER_AGENT'],"MSIE")) {
			    $filename = rawurlencode($filename);
			}
		    
		    //header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		    header('Content-Transfer-Encoding: binary');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		    header('Pragma: public');
			
			//header('Content-Disposition: attachment; filename="'. basename($file) . '"');
		    
			@ob_end_flush();
			flush();
			readfile($filePath);

            $this->eventLog('File downloaded: '.$filePath);
		    die();
		} else if(is_dir($filePath)) {
			throw new Exception('Downloading directories is not supported. Sorry for the inconvenience.');
		} else {
			header("HTTP/1.0 404 Not Found");
			throw new Exception('Unable to serve file. No such file or directory.');
		}
	}
	
	public function upload($tmpFile, $name) {
		$destination = '';
		$logMsg = "File uploaded: %s";

		if($this->getExt($name) == 'torrent' && $this->config['move_torrent_files'] !== false) {
			$destination = $this->config['move_torrent_files'];
			
			if(substr($this->config['move_torrent_files'], -1) != '/') {
				$destination .= '/';
			}

            $logMsg .= ' - moved to '.$this->config['move_torrent_files'];
		} else {
			$uploadDirectory = getcwd().$this->config['upload_dir'];
			
			if(substr($this->config['upload_dir'], -1) != '/') {
				$destination .= '/';
			}
			
			if($this->config['group_uploads_by_date'] == true) {
				$destination .= date('Y').'/'.date('F').'/';
			}
			
			if(file_exists($uploadDirectory.$destination.$name) && ($this->config['allow_overwrite'] === false)) {
				$name = $this->incrementFilename($uploadDirectory.$destination, $name);
			}

			if(!is_dir($uploadDirectory.$destination)) {
				mkdir($uploadDirectory.$destination, 0777, true);
			}
		}
		
		$destination .= $name;
		
		if(move_uploaded_file($tmpFile, $uploadDirectory.$destination) !== false) {
			$this->httpUrl = $this->getAppUrl().'?download='.substr($this->config['upload_dir'], 1).$destination; //Ninja
            $this->eventLog(sprintf($logMsg), $destination);

			return true;
		}

        $this->eventLog('Failed to upload file: '.$destination);
		return false;
	}		
    /**
     * Returns the path to the chosen theme directory
     *
     * @param bool $absolute Wether or not the path returned is absolute (default = false).
     * @return string Path to theme
     * @access public
     */
    public function getThemePath($absolute = false) {
        if ($absolute) {
            // Set the theme path
            $themePath = $this->appDir . '/themes/' . $this->themeName;
        } else {
            $realtivePath = $this->getRelativePath(getcwd(), $this->appDir);

            $themePath = $realtivePath . '/themes/' . $this->themeName;
        }

        return $themePath;
    }


    /**
     * Get an array of error messages or false when empty
     *
     * @return array|bool Array of error messages or false
     * @access public
     */
    public function getSystemMessages() {
        if (isset($this->systemMessage) && is_array($this->systemMessage)) {
            return $this->systemMessage;
        } else {
            return false;
        }
    }


    /**
     * Returns string of file size in human-readable format
     *
     * @param  string $filePath Path to file
     * @return string Human-readable file size
     * @access public
     */
    function getFileSize($filePath) {

        // Get file size
        $bytes = filesize($filePath);

        // Array of file size suffixes
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');

        // Calculate file size suffix factor
        $factor = floor((strlen($bytes) - 1) / 3);

        // Calculate the file size
        $fileSize = sprintf('%.2f', $bytes / pow(1024, $factor)) . $sizes[$factor];

        return $fileSize;

    }


    /**
     * Returns array of file hash values
     *
     * @param  string $path Path to file
     * @return array Array of file hashes
     * @access public
     */
    public function getFileHash($filePath) {

	$hashArray = array();

        // Verify file path exists and is a directory
        if (!file_exists($filePath)) {
            return json_encode($hashArray);
        }

        // Prevent access to hidden files
        if ($this->_isHidden($filePath)) {
            return json_encode($hashArray);
        }

        // Prevent access to parent folders
        if (strpos($filePath, '<') !== false || strpos($filePath, '>') !== false
        || strpos($filePath, '..') !== false || strpos($filePath, '/') === 0) {
            return json_encode($hashArray);
        }

        // Prevent hashing if file is too big
        if (filesize($filePath) > $this->config['hash_size_limit']) {
            $hashArray['md5']  = '[ File size exceeds threshold ]';
            $hashArray['sha1'] = '[ File size exceeds threshold ]';
        } else {
            $hashArray['md5']  = hash_file('md5', $filePath);
            $hashArray['sha1'] = hash_file('sha1', $filePath);

        }

        // Return the data
        return $hashArray;

    }


    /**
     * Set directory path variable
     *
     * @param string $path Path to directory
     * @return string Sanitizd path to directory
     * @access public
     */
    public function setDirectoryPath($path = null) {
        $this->directory = $this->setDirecoryPath($path);

        return $this->directory;

    }


    /**
     * Add a message to the system message array
     *
     * @param string $type The type of message (ie - error, success, notice, etc.)
     * @param string $message The message to be displayed to the user
     * @return bool true on success
     * @access public
     */
    public function setSystemMessage($type, $text) {

        // Create empty message array if it doesn't already exist
        if (isset($this->systemMessage) && !is_array($this->systemMessage)) {
            $this->systemMessage = array();
        }

        // Set the error message
        $this->systemMessage[] = array(
            'type'  => $type,
            'text'  => $text
        );

        return true;
    }


    /**
     * Validates and returns the directory path
     *
     * @param string @dir Directory path
     * @return string Directory path to be listed
     * @access protected
     */
    protected function setDirecoryPath($dir) {

        // Check for an empty variable
        if (empty($dir) || $dir == '.') {
            return '.';
        }

        // Eliminate double slashes
        while (strpos($dir, '//')) {
            $dir = str_replace('//', '/', $dir);
        }

        // Remove trailing slash if present
        if(substr($dir, -1, 1) == '/') {
            $dir = substr($dir, 0, -1);
        }

        // Verify file path exists and is a directory
        if (!file_exists($dir) || !is_dir($dir)) {
            $this->setSystemMessage('danger', '<b>ERROR:</b> File path does not exist');
            return '.';
        }

        // Prevent access to hidden files
        if ($this->_isHidden($dir)) {
            // Set the error message
            $this->setSystemMessage('danger', '<b>ERROR:</b> Access denied');

            // Set the directory to web root
            return '.';
        }

        // Prevent access to parent folders
        if (strpos($dir, '<') !== false || strpos($dir, '>') !== false || strpos($dir, '..') !== false || strpos($dir, '/') === 0) {
            $this->setSystemMessage('danger', '<b>ERROR:</b> An invalid path string was detected');
            return '.';
        } else {
            $directoryPath = $dir;
        }

        // Return
        return $directoryPath;
    }


    /**
     * Loop through directory and return array with file info, including
     * file path, size, modification time, icon and sort order.
     *
     * @param string $directory Directory path
     * @param @sort Sort method (default = natcase)
     * @return array Array of the directory contents
     * @access protected
     */
    protected function readDirectory($directory, $sort = 'natcase') {
        $directoryArray = array();

        $files = scandir($directory);

        // Read files/folders from the directory
        foreach ($files as $file) {
            if ($file != '.') {
                $relativePath = $directory . '/' . $file;

                if (substr($relativePath, 0, 2) == './') {
                    $relativePath = substr($relativePath, 2);
                }

                $realPath = realpath($relativePath);

                if (is_dir($realPath)) {
                    $iconClass = 'fa-folder';
                    $sort = 1;
                } else {
                    $fileExt = strtolower(pathinfo($realPath, PATHINFO_EXTENSION));

                    if (isset($this->fileTypes[$fileExt])) {
                        $iconClass = $this->fileTypes[$fileExt];
                    } else {
                        $iconClass = $this->fileTypes['blank'];
                    }

                    $sort = 2;
                }

                if ($file == '..') {
                    if ($this->directory != '.') {

                        $pathArray = explode('/', $relativePath);
                        unset($pathArray[count($pathArray)-1]);
                        unset($pathArray[count($pathArray)-1]);
                        $directoryPath = implode('/', $pathArray);

                        if (!empty($directoryPath)) {
                            $directoryPath = '?dir=' . urlencode($directoryPath);
                        }

                        $directoryArray['..'] = array(
                            'file_path'  => $this->appUrl . $directoryPath,
                            'file_size'  => '-',
                            'mod_time'   => date('Y-m-d H:i:s', filemtime($realPath)),
                            'icon_class' => 'fa-level-up',
                            'sort'       => 0
                        );
                    }
                } elseif (!$this->_isHidden($relativePath)) {

                    if (is_dir($relativePath)) {
                        $filePath = '?dir=' . urlencode($relativePath);
                    } else {
                        $filePath = $relativePath;
                    }

                    $directoryArray[pathinfo($relativePath, PATHINFO_BASENAME)] = array(
                        'file_path'  => $filePath,
                        'file_size'  => is_dir($realPath) ? '-' : $this->getFileSize($realPath),
                        'mod_time'   => date('Y-m-d H:i:s', filemtime($realPath)),
                        'icon_class' => $iconClass,
                        'sort'       => $sort
                    );
                }
            }
        }

        // Sort the array
        $reverseSort = in_array($this->directory, $this->config['reverse_sort']);
        $sortedArray = $this->arraySort($directoryArray, $this->config['list_sort_order'], $reverseSort);

        // Return the array
        return $sortedArray;

    }


    /**
     * Sorts an array by the provided sort method.
     *
     * @param array $array Array to be sorted
     * @param string $sortMethod Sorting method (acceptable inputs: natsort, natcasesort, etc.)
     * @param boolen $reverse Reverse the sorted array order if true (default = false)
     * @return array
     * @access protected
     */
    protected function arraySort($array, $sortMethod, $reverse = false) {
        // Create empty arrays
        $sortedArray = array();
        $finalArray  = array();

        // Create new array of just the keys and sort it
        $keys = array_keys($array);

        switch ($sortMethod) {
            case 'asort':
                asort($keys);
                break;
            case 'arsort':
                arsort($keys);
                break;
            case 'ksort':
                ksort($keys);
                break;
            case 'krsort':
                krsort($keys);
                break;
            case 'natcasesort':
                natcasesort($keys);
                break;
            case 'natsort':
                natsort($keys);
                break;
            case 'shuffle':
                shuffle($keys);
                break;
        }

        // Loop through the sorted values and move over the data
        if ($this->config['list_folders_first']) {

            foreach ($keys as $key) {
                if ($array[$key]['sort'] == 0) {
                    $sortedArray['0'][$key] = $array[$key];
                }
            }

            foreach ($keys as $key) {
                if ($array[$key]['sort'] == 1) {
                    $sortedArray[1][$key] = $array[$key];
                }
            }

            foreach ($keys as $key) {
                if ($array[$key]['sort'] == 2) {
                    $sortedArray[2][$key] = $array[$key];
                }
            }

            if ($reverse) {
                $sortedArray[1] = array_reverse($sortedArray[1]);
                $sortedArray[2] = array_reverse($sortedArray[2]);
            }

        } else {

            foreach ($keys as $key) {
                if ($array[$key]['sort'] == 0) {
                    $sortedArray[0][$key] = $array[$key];
                }
            }

            foreach ($keys as $key) {
                if ($array[$key]['sort'] > 0) {
                    $sortedArray[1][$key] = $array[$key];
                }
            }

            if ($reverse) {
                $sortedArray[1] = array_reverse($sortedArray[1]);
            }

        }

        foreach ($sortedArray as $array) {
            if (empty($array)) continue;
            foreach ($array as $key => $value) {
                $finalArray[$key] = $value;
            }
        }

        return $finalArray;

    }


    /**
     * Determines if a file is specified as hidden
     *
     * @param string $filePath Path to file to be checked if hidden
     * @return boolean Returns true if file is in hidden array, false if not
     * @access protected
     */
    protected function _isHidden($filePath) {

        // Add dot files to hidden files array
        if ($this->config['hide_dot_files']) {
            $this->config['hidden_files'][] = '.*';
        }

        // Compare path array to all hidden file paths
        foreach ($this->config['hidden_files'] as $hiddenPath) {
            if (fnmatch($hiddenPath, $filePath)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Builds the root application URL from server variables.
     *
     * @return string The application URL
     * @access protected
     */
    public function getAppUrl() {

        // Get the server protocol
        if (!empty($_SERVER['HTTPS'])) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        // Get the server hostname
        $host = $_SERVER['HTTP_HOST'];

        // Get the URL path
        $pathParts = pathinfo($_SERVER['PHP_SELF']);
        $path      = $pathParts['dirname'];

        // Remove backslash from path (Windows fix)
        if (substr($path, -1) == '\\') {
            $path = substr($path, 0, -1);
        }

        // Ensure the path ends with a forward slash
        if (substr($path, -1) != '/') {
            $path = $path . '/';
        }

        // Build the application URL
        $appUrl = $protocol . $host . $path;

        // Return the URL
        return $appUrl;
    }


    /**
      * Compares two paths and returns the relative path from one to the other
     *
     * @param string $fromPath Starting path
     * @param string $toPath Ending path
     * @return string $relativePath Relative path from $fromPath to $toPath
     * @access protected
     */
    protected function getRelativePath($fromPath, $toPath) {

        // Define the OS specific directory separator
        if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

        // Remove double slashes from path strings
        $fromPath = str_replace(DS . DS, DS, $fromPath);
        $toPath = str_replace(DS . DS, DS, $toPath);

        // Explode working dir and cache dir into arrays
        $fromPathArray = explode(DS, $fromPath);
        $toPathArray = explode(DS, $toPath);

        // Remove last fromPath array element if it's empty
        $x = count($fromPathArray) - 1;

        if(!trim($fromPathArray[$x])) {
            array_pop($fromPathArray);
        }

        // Remove last toPath array element if it's empty
        $x = count($toPathArray) - 1;

        if(!trim($toPathArray[$x])) {
            array_pop($toPathArray);
        }

        // Get largest array count
        $arrayMax = max(count($fromPathArray), count($toPathArray));

        // Set some default variables
        $diffArray = array();
        $samePath = true;
        $key = 1;

        // Generate array of the path differences
        while ($key <= $arrayMax) {

            // Get to path variable
            $toPath = isset($toPathArray[$key]) ? $toPathArray[$key] : null;

            // Get from path variable
            $fromPath = isset($fromPathArray[$key]) ? $fromPathArray[$key] : null;

            if ($toPath !== $fromPath || $samePath !== true) {

                // Prepend '..' for every level up that must be traversed
                if (isset($fromPathArray[$key])) {
                    array_unshift($diffArray, '..');
                }

                // Append directory name for every directory that must be traversed
                if (isset($toPathArray[$key])) {
                    $diffArray[] = $toPathArray[$key];
                }

                // Directory paths have diverged
                $samePath = false;
            }

            // Increment key
            $key++;
        }

        // Set the relative thumbnail directory path
        $relativePath = implode('/', $diffArray);

        // Return the relative path
        return $relativePath;

    }

}
